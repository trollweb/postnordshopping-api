# CommunicationSettingResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**type** | **string** |  | [optional] 
**channels** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannels**](CommunicationSettingStoreChannels.md) |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\CartItemStoreResponseLinks**](CartItemStoreResponseLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


