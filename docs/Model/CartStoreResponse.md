# CartStoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**ref** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**meta** | **object** |  | [optional] 
**items** | [**\Postnord\ShoppingApi\Model\CartItemStoreResponse[]**](CartItemStoreResponse.md) |  | [optional] 
**prefered** | **bool** |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\CartStoreResponseLinks**](CartStoreResponseLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


