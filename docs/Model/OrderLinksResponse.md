# OrderLinksResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rel** | **string** |  | [optional] 
**mime_type** | **string** |  | [optional] 
**uri** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


