# CartItemStoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**shop_org_number** | **string** |  | [optional] 
**shop_country_code** | **string** |  | [optional] 
**item_id** | **string** |  | [optional] 
**image_url** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**quantity** | **string** |  | [optional] 
**quantity_unit_code** | **string** |  | [optional] 
**quantity_unit_code_list_id** | **string** |  | [optional] 
**quantity_price** | **string** |  | [optional] 
**quantity_tax_price** | **string** |  | [optional] 
**quantity_price_currency_id** | **string** |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\CartItemStoreResponseLinks**](CartItemStoreResponseLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


