# GroupOrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prepaid** | **int** |  | [optional] 
**total_price** | **int** |  | [optional] 
**total_tax_price** | **int** |  | [optional] 
**total_price_without_tax** | **int** |  | [optional] 
**total_freight_price** | **int** |  | [optional] 
**payment_method** | **string** |  | [optional] 
**confirmation_number** | **string** |  | [optional] 
**created_at** | **string** |  | [optional] 
**updated_at** | **string** |  | [optional] 
**consignee** | [**\Postnord\ShoppingApi\Model\GroupOrderResponseConsignee**](GroupOrderResponseConsignee.md) |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\OrderGroupLinksResponse[]**](OrderGroupLinksResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


