# AddressStore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**street_name** | **string** |  | [optional] 
**additional_street_name** | **string** |  | [optional] 
**city_name** | **string** |  | [optional] 
**postal_zone** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**delivery_instructions** | **string** |  | [optional] 
**pickup_point_id** | **string** |  | [optional] 
**delivery_method** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


