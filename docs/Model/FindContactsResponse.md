# FindContactsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Postnord\ShoppingApi\Model\ContactResponse**](ContactResponse.md) |  | [optional] 
**meta** | [**\Postnord\ShoppingApi\Model\Meta**](Meta.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


