# GroupOrderResponseConsigneeAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **string** |  | [optional] 
**city_name** | **string** |  | [optional] 
**postal_zone** | **string** |  | [optional] 
**street_name** | **string** |  | [optional] 
**additional_street_name** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


