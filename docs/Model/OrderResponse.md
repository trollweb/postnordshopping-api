# OrderResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**shop_org_number** | **string** |  | [optional] 
**shop_country_code** | **string** |  | [optional] 
**prepaid** | **int** |  | [optional] 
**total_price** | **int** |  | [optional] 
**total_price_without_tax** | **int** |  | [optional] 
**total_tax_price** | **int** |  | [optional] 
**total_freight_price** | **int** |  | [optional] 
**created_at** | **string** |  | [optional] 
**updated_at** | **string** |  | [optional] 
**fetched_at** | **string** |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\OrderLinksResponse[]**](OrderLinksResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


