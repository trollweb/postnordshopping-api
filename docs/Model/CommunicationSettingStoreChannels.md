# CommunicationSettingStoreChannels

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sms** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannelsSms**](CommunicationSettingStoreChannelsSms.md) |  | [optional] 
**email** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannelsSms**](CommunicationSettingStoreChannelsSms.md) |  | [optional] 
**push** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannelsSms**](CommunicationSettingStoreChannelsSms.md) |  | [optional] 
**voice** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannelsSms**](CommunicationSettingStoreChannelsSms.md) |  | [optional] 
**print** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannelsSms**](CommunicationSettingStoreChannelsSms.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


