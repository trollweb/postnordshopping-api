# GroupOrderResponseConsignee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**id** | **string** |  | [optional] 
**address** | [**\Postnord\ShoppingApi\Model\GroupOrderResponseConsigneeAddress**](GroupOrderResponseConsigneeAddress.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


