# CommunicationSettingStore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**channels** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStoreChannels**](CommunicationSettingStoreChannels.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


