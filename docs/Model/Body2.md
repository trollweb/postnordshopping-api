# Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shop_org_number** | **string** |  | [optional] 
**shop_country_code** | **string** |  | [optional] 
**item_id** | **string** |  | [optional] 
**image_url** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**quantity** | **int** |  | [optional] 
**quantity_unit_code** | **string** |  | [optional] 
**quantity_unit_code_list_id** | **string** |  | [optional] 
**quantity_price** | **int** |  | [optional] 
**quantity_tax_price** | **int** |  | [optional] 
**quantity_price_currency_id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


