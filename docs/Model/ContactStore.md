# ContactStore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**firstname** | **string** |  | [optional] 
**middlename** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**birth_date** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**mobile** | **string** |  | [optional] 
**mobile_country_code** | **string** |  | [optional] 
**credit** | **int** |  | [optional] 
**photo** | **string** |  | [optional] 
**toc_agreed_at** | **string** |  | [optional] 
**locale** | **string** |  | [optional] 
**gender** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


