# ContactAddressResponseDeliverySchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**monday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**tuesday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**wednesday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**thursday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**friday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**saturday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 
**sunday** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday**](ContactAddressResponseDeliveryScheduleMonday.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


