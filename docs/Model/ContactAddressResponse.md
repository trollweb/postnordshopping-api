# ContactAddressResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**street_name** | **string** |  | [optional] 
**additional_street_name** | **string** |  | [optional] 
**city_name** | **string** |  | [optional] 
**postal_zone** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**prefered** | **bool** |  | [optional] 
**delivery_method** | **string** |  | [optional] 
**delivery_instructions** | **string** |  | [optional] 
**pickup_point_id** | **string** |  | [optional] 
**delivery_schedule** | [**\Postnord\ShoppingApi\Model\ContactAddressResponseDeliverySchedule**](ContactAddressResponseDeliverySchedule.md) |  | [optional] 
**links** | [**\Postnord\ShoppingApi\Model\CartItemStoreResponseLinks**](CartItemStoreResponseLinks.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


