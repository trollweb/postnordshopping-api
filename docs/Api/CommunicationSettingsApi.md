# Postnord\ShoppingApi\CommunicationSettingsApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiContactsIdCommunicationSettingsCommunicationSettingIdDelete**](CommunicationSettingsApi.md#apiContactsIdCommunicationSettingsCommunicationSettingIdDelete) | **DELETE** /api/contacts/{id}/communication-settings/{communicationSettingId} | Delete the given communication setting
[**apiContactsIdCommunicationSettingsCommunicationSettingIdPut**](CommunicationSettingsApi.md#apiContactsIdCommunicationSettingsCommunicationSettingIdPut) | **PUT** /api/contacts/{id}/communication-settings/{communicationSettingId} | Update the given communication setting for a contact
[**apiContactsIdCommunicationSettingsPost**](CommunicationSettingsApi.md#apiContactsIdCommunicationSettingsPost) | **POST** /api/contacts/{id}/communication-settings | Add a new communication setting for a contact


# **apiContactsIdCommunicationSettingsCommunicationSettingIdDelete**
> apiContactsIdCommunicationSettingsCommunicationSettingIdDelete($id, $communication_setting_id)

Delete the given communication setting

Delete the given communication setting

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CommunicationSettingsApi();
$id = "id_example"; // string | Unique identifier for the contact
$communication_setting_id = "communication_setting_id_example"; // string | Unique identifier for the communication setting

try {
    $api_instance->apiContactsIdCommunicationSettingsCommunicationSettingIdDelete($id, $communication_setting_id);
} catch (Exception $e) {
    echo 'Exception when calling CommunicationSettingsApi->apiContactsIdCommunicationSettingsCommunicationSettingIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **communication_setting_id** | **string**| Unique identifier for the communication setting |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCommunicationSettingsCommunicationSettingIdPut**
> apiContactsIdCommunicationSettingsCommunicationSettingIdPut($id, $communication_setting_id, $body)

Update the given communication setting for a contact

Update the given communication setting for a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CommunicationSettingsApi();
$id = "id_example"; // string | Unique identifier for the contact
$communication_setting_id = "communication_setting_id_example"; // string | Unique identifier for the communication setting
$body = new \Postnord\ShoppingApi\Model\CommunicationSettingUpdate(); // \Postnord\ShoppingApi\Model\CommunicationSettingUpdate | Communication setting attributes

try {
    $api_instance->apiContactsIdCommunicationSettingsCommunicationSettingIdPut($id, $communication_setting_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling CommunicationSettingsApi->apiContactsIdCommunicationSettingsCommunicationSettingIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **communication_setting_id** | **string**| Unique identifier for the communication setting |
 **body** | [**\Postnord\ShoppingApi\Model\CommunicationSettingUpdate**](../Model/\Postnord\ShoppingApi\Model\CommunicationSettingUpdate.md)| Communication setting attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCommunicationSettingsPost**
> \Postnord\ShoppingApi\Model\InlineResponse2014 apiContactsIdCommunicationSettingsPost($id, $body)

Add a new communication setting for a contact

Add a new communication setting for a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CommunicationSettingsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\CommunicationSettingStore(); // \Postnord\ShoppingApi\Model\CommunicationSettingStore | Communication setting attributes

try {
    $result = $api_instance->apiContactsIdCommunicationSettingsPost($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CommunicationSettingsApi->apiContactsIdCommunicationSettingsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\CommunicationSettingStore**](../Model/\Postnord\ShoppingApi\Model\CommunicationSettingStore.md)| Communication setting attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2014**](../Model/InlineResponse2014.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

