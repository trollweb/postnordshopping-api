# Postnord\ShoppingApi\CustomerOrdersApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetchOrders**](CustomerOrdersApi.md#fetchOrders) | **GET** /api/customers/{id}/orders/{orderId}/fetch | Fetch the given order
[**orders**](CustomerOrdersApi.md#orders) | **GET** /api/customers/{id}/orders | List customer orders
[**updateCanceledOrders**](CustomerOrdersApi.md#updateCanceledOrders) | **DELETE** /api/customers/{id}/orders/{orderId}/status | Update order canceled status.
[**updateSentOrders**](CustomerOrdersApi.md#updateSentOrders) | **POST** /api/customers/{id}/orders/{orderId}/status | Update order sent status.


# **fetchOrders**
> fetchOrders($id, $order_id)

Fetch the given order

Fetches the given order and set the fetched flag.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CustomerOrdersApi();
$id = "id_example"; // string | Customer id
$order_id = "order_id_example"; // string | Customer id

try {
    $api_instance->fetchOrders($id, $order_id);
} catch (Exception $e) {
    echo 'Exception when calling CustomerOrdersApi->fetchOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Customer id |
 **order_id** | **string**| Customer id |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **orders**
> \Postnord\ShoppingApi\Model\InlineResponse2001 orders($id, $fetched_at, $includes)

List customer orders

List all orders for the given customer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CustomerOrdersApi();
$id = "id_example"; // string | Customer id
$fetched_at = "fetched_at_example"; // string | Accepts a datetime or null
$includes = "includes_example"; // string | Relationships to include in the response, available includes: lines

try {
    $result = $api_instance->orders($id, $fetched_at, $includes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerOrdersApi->orders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Customer id |
 **fetched_at** | **string**| Accepts a datetime or null | [optional]
 **includes** | **string**| Relationships to include in the response, available includes: lines | [optional]

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2001**](../Model/InlineResponse2001.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateCanceledOrders**
> updateCanceledOrders($id, $order_id, $body)

Update order canceled status.

Update the given items's canceled status.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CustomerOrdersApi();
$id = "id_example"; // string | Customer id
$order_id = "order_id_example"; // string | Order id
$body = new \Postnord\ShoppingApi\Model\Body9(); // \Postnord\ShoppingApi\Model\Body9 | Item attributes

try {
    $api_instance->updateCanceledOrders($id, $order_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling CustomerOrdersApi->updateCanceledOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Customer id |
 **order_id** | **string**| Order id |
 **body** | [**\Postnord\ShoppingApi\Model\Body9**](../Model/\Postnord\ShoppingApi\Model\Body9.md)| Item attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateSentOrders**
> updateSentOrders($id, $order_id, $body)

Update order sent status.

Update the given items's sent status.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CustomerOrdersApi();
$id = "id_example"; // string | Customer id
$order_id = "order_id_example"; // string | Order id
$body = new \Postnord\ShoppingApi\Model\Body8(); // \Postnord\ShoppingApi\Model\Body8 | Item attributes

try {
    $api_instance->updateSentOrders($id, $order_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling CustomerOrdersApi->updateSentOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Customer id |
 **order_id** | **string**| Order id |
 **body** | [**\Postnord\ShoppingApi\Model\Body8**](../Model/\Postnord\ShoppingApi\Model\Body8.md)| Item attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

