# Postnord\ShoppingApi\OrdersApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiContactsIdCartsCartIdOrderPost**](OrdersApi.md#apiContactsIdCartsCartIdOrderPost) | **POST** /api/contacts/{id}/carts/{cartId}/order | Convert the given cart to an order


# **apiContactsIdCartsCartIdOrderPost**
> \Postnord\ShoppingApi\Model\InlineResponse200 apiContactsIdCartsCartIdOrderPost($id, $cart_id, $body)

Convert the given cart to an order

This will convert the given cart to an order instance and clear out the cart.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\OrdersApi();
$id = "id_example"; // string | Unique identifier for the contact
$cart_id = "cart_id_example"; // string | Unique identifier for the cart
$body = new \Postnord\ShoppingApi\Model\Body3(); // \Postnord\ShoppingApi\Model\Body3 | Item attributes

try {
    $result = $api_instance->apiContactsIdCartsCartIdOrderPost($id, $cart_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->apiContactsIdCartsCartIdOrderPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **cart_id** | **string**| Unique identifier for the cart |
 **body** | [**\Postnord\ShoppingApi\Model\Body3**](../Model/\Postnord\ShoppingApi\Model\Body3.md)| Item attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

