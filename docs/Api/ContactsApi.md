# Postnord\ShoppingApi\ContactsApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiContactsIdDelete**](ContactsApi.md#apiContactsIdDelete) | **DELETE** /api/contacts/{id} | Delete the given contact
[**apiContactsIdDirectMessagePatch**](ContactsApi.md#apiContactsIdDirectMessagePatch) | **PATCH** /api/contacts/{id}/direct-message | Set the direct message flag
[**apiContactsIdGet**](ContactsApi.md#apiContactsIdGet) | **GET** /api/contacts/{id} | Show contact
[**apiContactsIdPaymentPatch**](ContactsApi.md#apiContactsIdPaymentPatch) | **PATCH** /api/contacts/{id}/payment | Update payment settings
[**apiContactsIdPhotoPost**](ContactsApi.md#apiContactsIdPhotoPost) | **POST** /api/contacts/{id}/photo | Update the contacts photo
[**apiContactsIdPut**](ContactsApi.md#apiContactsIdPut) | **PUT** /api/contacts/{id} | Update an existing contact
[**apiContactsIdSignaturePost**](ContactsApi.md#apiContactsIdSignaturePost) | **POST** /api/contacts/{id}/signature | Update the contacts signature
[**apiContactsIdTocPatch**](ContactsApi.md#apiContactsIdTocPatch) | **PATCH** /api/contacts/{id}/toc | Confirm the terms of condition
[**apiContactsIdVerifyPost**](ContactsApi.md#apiContactsIdVerifyPost) | **POST** /api/contacts/{id}/verify | Mark the contact as verified
[**apiContactsPost**](ContactsApi.md#apiContactsPost) | **POST** /api/contacts | Add a new contact
[**contacts**](ContactsApi.md#contacts) | **GET** /api/contacts | Find contacts


# **apiContactsIdDelete**
> apiContactsIdDelete($id)

Delete the given contact

Delete the given contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact

try {
    $api_instance->apiContactsIdDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdDirectMessagePatch**
> apiContactsIdDirectMessagePatch($id, $body)

Set the direct message flag

Set the direct message flag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\Body4(); // \Postnord\ShoppingApi\Model\Body4 | Payload

try {
    $api_instance->apiContactsIdDirectMessagePatch($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdDirectMessagePatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\Body4**](../Model/\Postnord\ShoppingApi\Model\Body4.md)| Payload | [optional]

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdGet**
> \Postnord\ShoppingApi\Model\InlineResponse2011 apiContactsIdGet($id, $includes)

Show contact

Show a single contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$includes = "includes_example"; // string | Relationships to include, available includes: addresses,communicationSettings,carts,givenProxies,allowedProxies

try {
    $result = $api_instance->apiContactsIdGet($id, $includes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **includes** | **string**| Relationships to include, available includes: addresses,communicationSettings,carts,givenProxies,allowedProxies | [optional]

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdPaymentPatch**
> apiContactsIdPaymentPatch($id, $body)

Update payment settings

Update payment settings

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\Body5(); // \Postnord\ShoppingApi\Model\Body5 | Payload

try {
    $api_instance->apiContactsIdPaymentPatch($id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdPaymentPatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\Body5**](../Model/\Postnord\ShoppingApi\Model\Body5.md)| Payload |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdPhotoPost**
> \Postnord\ShoppingApi\Model\InlineResponse2011 apiContactsIdPhotoPost($id, $body)

Update the contacts photo

Update the contacts photo

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\Body6(); // \Postnord\ShoppingApi\Model\Body6 | Photo attributes

try {
    $result = $api_instance->apiContactsIdPhotoPost($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdPhotoPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\Body6**](../Model/\Postnord\ShoppingApi\Model\Body6.md)| Photo attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdPut**
> \Postnord\ShoppingApi\Model\InlineResponse2011 apiContactsIdPut($id, $body)

Update an existing contact

Update an existing contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\ContactStore(); // \Postnord\ShoppingApi\Model\ContactStore | Contact attributes

try {
    $result = $api_instance->apiContactsIdPut($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\ContactStore**](../Model/\Postnord\ShoppingApi\Model\ContactStore.md)| Contact attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdSignaturePost**
> \Postnord\ShoppingApi\Model\InlineResponse2011 apiContactsIdSignaturePost($id, $body)

Update the contacts signature

Update the contacts signature

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\Body7(); // \Postnord\ShoppingApi\Model\Body7 | Signature attributes

try {
    $result = $api_instance->apiContactsIdSignaturePost($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdSignaturePost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\Body7**](../Model/\Postnord\ShoppingApi\Model\Body7.md)| Signature attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdTocPatch**
> apiContactsIdTocPatch($id)

Confirm the terms of condition

Confirm the terms of condition

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact

try {
    $api_instance->apiContactsIdTocPatch($id);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdTocPatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdVerifyPost**
> apiContactsIdVerifyPost($id)

Mark the contact as verified

Mark the contact as verified

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$id = "id_example"; // string | Unique identifier for the contact

try {
    $api_instance->apiContactsIdVerifyPost($id);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsIdVerifyPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsPost**
> \Postnord\ShoppingApi\Model\InlineResponse2011 apiContactsPost($body)

Add a new contact

Add a new contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$body = new \Postnord\ShoppingApi\Model\ContactStore(); // \Postnord\ShoppingApi\Model\ContactStore | Contact attributes

try {
    $result = $api_instance->apiContactsPost($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->apiContactsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Postnord\ShoppingApi\Model\ContactStore**](../Model/\Postnord\ShoppingApi\Model\ContactStore.md)| Contact attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2011**](../Model/InlineResponse2011.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **contacts**
> \Postnord\ShoppingApi\Model\FindContactsResponse contacts($strict, $firstname, $lastname, $middlename, $mobile, $mobile_contry_code, $email, $birth_date, $credit, $includes)

Find contacts

Find contacts by performing query against the different available attributes.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactsApi();
$strict = true; // bool | Disable strict compare
$firstname = "firstname_example"; // string | Firstname for the contact
$lastname = "lastname_example"; // string | Lastname for the contact
$middlename = "middlename_example"; // string | Middlename for the contact
$mobile = "mobile_example"; // string | Mobile phone number
$mobile_contry_code = "mobile_contry_code_example"; // string | Mobile country code
$email = "email_example"; // string | Email address for the contact
$birth_date = "birth_date_example"; // string | Birth date for the contact
$credit = 56; // int | Credit limit for the contact
$includes = "includes_example"; // string | Relationships to include in the response, available includes: addresses,communicationSettings,carts,givenProxies,allowedProxies

try {
    $result = $api_instance->contacts($strict, $firstname, $lastname, $middlename, $mobile, $mobile_contry_code, $email, $birth_date, $credit, $includes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->contacts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **strict** | **bool**| Disable strict compare | [optional]
 **firstname** | **string**| Firstname for the contact | [optional]
 **lastname** | **string**| Lastname for the contact | [optional]
 **middlename** | **string**| Middlename for the contact | [optional]
 **mobile** | **string**| Mobile phone number | [optional]
 **mobile_contry_code** | **string**| Mobile country code | [optional]
 **email** | **string**| Email address for the contact | [optional]
 **birth_date** | **string**| Birth date for the contact | [optional]
 **credit** | **int**| Credit limit for the contact | [optional]
 **includes** | **string**| Relationships to include in the response, available includes: addresses,communicationSettings,carts,givenProxies,allowedProxies | [optional]

### Return type

[**\Postnord\ShoppingApi\Model\FindContactsResponse**](../Model/FindContactsResponse.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

