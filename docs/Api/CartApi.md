# Postnord\ShoppingApi\CartApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiContactsIdCartsCartIdDelete**](CartApi.md#apiContactsIdCartsCartIdDelete) | **DELETE** /api/contacts/{id}/carts/{cartId} | Delete the given cart
[**apiContactsIdCartsCartIdItemsItemIdDelete**](CartApi.md#apiContactsIdCartsCartIdItemsItemIdDelete) | **DELETE** /api/contacts/{id}/carts/{cartId}/items/{itemId} | Delete the given item
[**apiContactsIdCartsCartIdItemsItemIdPatch**](CartApi.md#apiContactsIdCartsCartIdItemsItemIdPatch) | **PATCH** /api/contacts/{id}/carts/{cartId}/items/{itemId} | Update the given item
[**apiContactsIdCartsCartIdItemsPost**](CartApi.md#apiContactsIdCartsCartIdItemsPost) | **POST** /api/contacts/{id}/carts/{cartId}/items | Add an item to the given cart
[**apiContactsIdCartsPost**](CartApi.md#apiContactsIdCartsPost) | **POST** /api/contacts/{id}/carts | Create a new cart for the given contact


# **apiContactsIdCartsCartIdDelete**
> apiContactsIdCartsCartIdDelete($id, $cart_id)

Delete the given cart

Delete the given cart

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CartApi();
$id = "id_example"; // string | Unique identifier for the contact
$cart_id = "cart_id_example"; // string | Unique identifier for the cart

try {
    $api_instance->apiContactsIdCartsCartIdDelete($id, $cart_id);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->apiContactsIdCartsCartIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **cart_id** | **string**| Unique identifier for the cart |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCartsCartIdItemsItemIdDelete**
> apiContactsIdCartsCartIdItemsItemIdDelete($id, $cart_id, $item_id)

Delete the given item

Delete the given item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CartApi();
$id = "id_example"; // string | Unique identifier for the contact
$cart_id = "cart_id_example"; // string | Unique identifier for the cart
$item_id = "item_id_example"; // string | Unique identifier for the item in the cart

try {
    $api_instance->apiContactsIdCartsCartIdItemsItemIdDelete($id, $cart_id, $item_id);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->apiContactsIdCartsCartIdItemsItemIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **cart_id** | **string**| Unique identifier for the cart |
 **item_id** | **string**| Unique identifier for the item in the cart |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCartsCartIdItemsItemIdPatch**
> apiContactsIdCartsCartIdItemsItemIdPatch($id, $cart_id, $item_id, $body)

Update the given item

Update the given item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CartApi();
$id = "id_example"; // string | Unique identifier for the contact
$cart_id = "cart_id_example"; // string | Unique identifier for the cart
$item_id = "item_id_example"; // string | Unique identifier for the item in the cart
$body = new \Postnord\ShoppingApi\Model\Body2(); // \Postnord\ShoppingApi\Model\Body2 | Item attributes

try {
    $api_instance->apiContactsIdCartsCartIdItemsItemIdPatch($id, $cart_id, $item_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->apiContactsIdCartsCartIdItemsItemIdPatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **cart_id** | **string**| Unique identifier for the cart |
 **item_id** | **string**| Unique identifier for the item in the cart |
 **body** | [**\Postnord\ShoppingApi\Model\Body2**](../Model/\Postnord\ShoppingApi\Model\Body2.md)| Item attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCartsCartIdItemsPost**
> \Postnord\ShoppingApi\Model\InlineResponse2013 apiContactsIdCartsCartIdItemsPost($id, $cart_id, $body)

Add an item to the given cart

Add an item to the given cart

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CartApi();
$id = "id_example"; // string | Unique identifier for the contact
$cart_id = "cart_id_example"; // string | Unique identifier for the cart
$body = new \Postnord\ShoppingApi\Model\Body1(); // \Postnord\ShoppingApi\Model\Body1 | Item attributes

try {
    $result = $api_instance->apiContactsIdCartsCartIdItemsPost($id, $cart_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->apiContactsIdCartsCartIdItemsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **cart_id** | **string**| Unique identifier for the cart |
 **body** | [**\Postnord\ShoppingApi\Model\Body1**](../Model/\Postnord\ShoppingApi\Model\Body1.md)| Item attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2013**](../Model/InlineResponse2013.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdCartsPost**
> \Postnord\ShoppingApi\Model\InlineResponse2012 apiContactsIdCartsPost($id, $body)

Create a new cart for the given contact

Create a new cart for the given contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\CartApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\Body(); // \Postnord\ShoppingApi\Model\Body | Cart attributes

try {
    $result = $api_instance->apiContactsIdCartsPost($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CartApi->apiContactsIdCartsPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\Body**](../Model/\Postnord\ShoppingApi\Model\Body.md)| Cart attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse2012**](../Model/InlineResponse2012.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

