# Postnord\ShoppingApi\ContactAddressesApi

All URIs are relative to *https://postnord-contacts.empatix.no/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiConctactsIdAddressesAddressIdDelete**](ContactAddressesApi.md#apiConctactsIdAddressesAddressIdDelete) | **DELETE** /api/conctacts/{id}/addresses/{addressId} | Delete the given address
[**apiConctactsIdAddressesAddressIdPut**](ContactAddressesApi.md#apiConctactsIdAddressesAddressIdPut) | **PUT** /api/conctacts/{id}/addresses/{addressId} | Update an existing address for a contact
[**apiContactsIdAddressesAddressIdDeliverySchedulePut**](ContactAddressesApi.md#apiContactsIdAddressesAddressIdDeliverySchedulePut) | **PUT** /api/contacts/{id}/addresses/{addressId}/delivery-schedule | Set the delivery schedule for a given address
[**apiContactsIdAddressesAddressIdPreferedPatch**](ContactAddressesApi.md#apiContactsIdAddressesAddressIdPreferedPatch) | **PATCH** /api/contacts/{id}/addresses/{addressId}/prefered | Set the prefered address
[**apiContactsIdAddressesPost**](ContactAddressesApi.md#apiContactsIdAddressesPost) | **POST** /api/contacts/{id}/addresses | Add a new address to an existing contact


# **apiConctactsIdAddressesAddressIdDelete**
> apiConctactsIdAddressesAddressIdDelete($id, $address_id)

Delete the given address

Delete the given address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactAddressesApi();
$id = "id_example"; // string | Unique identifier for the contact
$address_id = "address_id_example"; // string | Unique identifier for the address

try {
    $api_instance->apiConctactsIdAddressesAddressIdDelete($id, $address_id);
} catch (Exception $e) {
    echo 'Exception when calling ContactAddressesApi->apiConctactsIdAddressesAddressIdDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **address_id** | **string**| Unique identifier for the address |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiConctactsIdAddressesAddressIdPut**
> \Postnord\ShoppingApi\Model\InlineResponse201 apiConctactsIdAddressesAddressIdPut($id, $address_id, $body)

Update an existing address for a contact

Update an existing address for a contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactAddressesApi();
$id = "id_example"; // string | Unique identifier for the contact
$address_id = "address_id_example"; // string | Unique identifier for the address
$body = new \Postnord\ShoppingApi\Model\AddressStore(); // \Postnord\ShoppingApi\Model\AddressStore | Address attributes

try {
    $result = $api_instance->apiConctactsIdAddressesAddressIdPut($id, $address_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactAddressesApi->apiConctactsIdAddressesAddressIdPut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **address_id** | **string**| Unique identifier for the address |
 **body** | [**\Postnord\ShoppingApi\Model\AddressStore**](../Model/\Postnord\ShoppingApi\Model\AddressStore.md)| Address attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdAddressesAddressIdDeliverySchedulePut**
> apiContactsIdAddressesAddressIdDeliverySchedulePut($id, $address_id, $body)

Set the delivery schedule for a given address

Set the delivery schedule for a given address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactAddressesApi();
$id = "id_example"; // string | Unique identifier for the contact
$address_id = "address_id_example"; // string | Unique identifier for the address
$body = new \Postnord\ShoppingApi\Model\DeliveryScheduleUpdate(); // \Postnord\ShoppingApi\Model\DeliveryScheduleUpdate | Delivery schedule attributes

try {
    $api_instance->apiContactsIdAddressesAddressIdDeliverySchedulePut($id, $address_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ContactAddressesApi->apiContactsIdAddressesAddressIdDeliverySchedulePut: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **address_id** | **string**| Unique identifier for the address |
 **body** | [**\Postnord\ShoppingApi\Model\DeliveryScheduleUpdate**](../Model/\Postnord\ShoppingApi\Model\DeliveryScheduleUpdate.md)| Delivery schedule attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdAddressesAddressIdPreferedPatch**
> apiContactsIdAddressesAddressIdPreferedPatch($id, $address_id, $body)

Set the prefered address

Set the prefered address

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactAddressesApi();
$id = "id_example"; // string | Unique identifier for the contact
$address_id = "address_id_example"; // string | Unique identifier for the address
$body = new \Postnord\ShoppingApi\Model\AddressStore(); // \Postnord\ShoppingApi\Model\AddressStore | Address attributes

try {
    $api_instance->apiContactsIdAddressesAddressIdPreferedPatch($id, $address_id, $body);
} catch (Exception $e) {
    echo 'Exception when calling ContactAddressesApi->apiContactsIdAddressesAddressIdPreferedPatch: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **address_id** | **string**| Unique identifier for the address |
 **body** | [**\Postnord\ShoppingApi\Model\AddressStore**](../Model/\Postnord\ShoppingApi\Model\AddressStore.md)| Address attributes |

### Return type

void (empty response body)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **apiContactsIdAddressesPost**
> \Postnord\ShoppingApi\Model\InlineResponse201 apiContactsIdAddressesPost($id, $body)

Add a new address to an existing contact

Add a new address to an existing contact

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth
Postnord\ShoppingApi\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$api_instance = new Postnord\ShoppingApi\Api\ContactAddressesApi();
$id = "id_example"; // string | Unique identifier for the contact
$body = new \Postnord\ShoppingApi\Model\AddressStore(); // \Postnord\ShoppingApi\Model\AddressStore | Address attributes

try {
    $result = $api_instance->apiContactsIdAddressesPost($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactAddressesApi->apiContactsIdAddressesPost: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| Unique identifier for the contact |
 **body** | [**\Postnord\ShoppingApi\Model\AddressStore**](../Model/\Postnord\ShoppingApi\Model\AddressStore.md)| Address attributes |

### Return type

[**\Postnord\ShoppingApi\Model\InlineResponse201**](../Model/InlineResponse201.md)

### Authorization

[oauth](../../README.md#oauth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

