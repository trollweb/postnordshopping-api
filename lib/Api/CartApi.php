<?php
/**
 * CartApi
 * PHP version 5
 *
 * @category Class
 * @package  Postnord\ShoppingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * oAuth and Contacts APIs
 *
 * Api enpoints for oAuth and Contacts
 *
 * OpenAPI spec version: 1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Postnord\ShoppingApi\Api;

use \Postnord\ShoppingApi\Configuration;
use \Postnord\ShoppingApi\ApiClient;
use \Postnord\ShoppingApi\ApiException;
use \Postnord\ShoppingApi\ObjectSerializer;

/**
 * CartApi Class Doc Comment
 *
 * @category Class
 * @package  Postnord\ShoppingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class CartApi
{

    /**
     * API Client
     *
     * @var \Postnord\ShoppingApi\ApiClient instance of the ApiClient
     */
    protected $apiClient;

    /**
     * Constructor
     *
     * @param \Postnord\ShoppingApi\ApiClient|null $apiClient The api client to use
     */
    public function __construct(\Postnord\ShoppingApi\ApiClient $apiClient = null)
    {
        if ($apiClient == null) {
            $apiClient = new ApiClient();
            $apiClient->getConfig()->setHost('https://postnord-contacts.empatix.no/');
        }

        $this->apiClient = $apiClient;
    }

    /**
     * Get API client
     *
     * @return \Postnord\ShoppingApi\ApiClient get the API client
     */
    public function getApiClient()
    {
        return $this->apiClient;
    }

    /**
     * Set the API client
     *
     * @param \Postnord\ShoppingApi\ApiClient $apiClient set the API client
     *
     * @return CartApi
     */
    public function setApiClient(\Postnord\ShoppingApi\ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        return $this;
    }

    /**
     * Operation apiContactsIdCartsCartIdDelete
     *
     * Delete the given cart
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @return void
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdDelete($id, $cart_id)
    {
        list($response) = $this->apiContactsIdCartsCartIdDeleteWithHttpInfo($id, $cart_id);
        return $response;
    }

    /**
     * Operation apiContactsIdCartsCartIdDeleteWithHttpInfo
     *
     * Delete the given cart
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @return Array of null, HTTP status code, HTTP response headers (array of strings)
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdDeleteWithHttpInfo($id, $cart_id)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling apiContactsIdCartsCartIdDelete');
        }
        // verify the required parameter 'cart_id' is set
        if ($cart_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $cart_id when calling apiContactsIdCartsCartIdDelete');
        }
        // parse inputs
        $resourcePath = "/api/contacts/{id}/carts/{cartId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($cart_id !== null) {
            $resourcePath = str_replace(
                "{" . "cartId" . "}",
                $this->apiClient->getSerializer()->toPathValue($cart_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'DELETE',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/api/contacts/{id}/carts/{cartId}'
            );

            return array(null, $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsItemIdDelete
     *
     * Delete the given item
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param string $item_id Unique identifier for the item in the cart (required)
     * @return void
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsItemIdDelete($id, $cart_id, $item_id)
    {
        list($response) = $this->apiContactsIdCartsCartIdItemsItemIdDeleteWithHttpInfo($id, $cart_id, $item_id);
        return $response;
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsItemIdDeleteWithHttpInfo
     *
     * Delete the given item
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param string $item_id Unique identifier for the item in the cart (required)
     * @return Array of null, HTTP status code, HTTP response headers (array of strings)
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsItemIdDeleteWithHttpInfo($id, $cart_id, $item_id)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling apiContactsIdCartsCartIdItemsItemIdDelete');
        }
        // verify the required parameter 'cart_id' is set
        if ($cart_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $cart_id when calling apiContactsIdCartsCartIdItemsItemIdDelete');
        }
        // verify the required parameter 'item_id' is set
        if ($item_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $item_id when calling apiContactsIdCartsCartIdItemsItemIdDelete');
        }
        // parse inputs
        $resourcePath = "/api/contacts/{id}/carts/{cartId}/items/{itemId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($cart_id !== null) {
            $resourcePath = str_replace(
                "{" . "cartId" . "}",
                $this->apiClient->getSerializer()->toPathValue($cart_id),
                $resourcePath
            );
        }
        // path params
        if ($item_id !== null) {
            $resourcePath = str_replace(
                "{" . "itemId" . "}",
                $this->apiClient->getSerializer()->toPathValue($item_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        
        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'DELETE',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/api/contacts/{id}/carts/{cartId}/items/{itemId}'
            );

            return array(null, $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsItemIdPatch
     *
     * Update the given item
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param string $item_id Unique identifier for the item in the cart (required)
     * @param \Postnord\ShoppingApi\Model\Body2 $body Item attributes (required)
     * @return void
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsItemIdPatch($id, $cart_id, $item_id, $body)
    {
        list($response) = $this->apiContactsIdCartsCartIdItemsItemIdPatchWithHttpInfo($id, $cart_id, $item_id, $body);
        return $response;
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsItemIdPatchWithHttpInfo
     *
     * Update the given item
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param string $item_id Unique identifier for the item in the cart (required)
     * @param \Postnord\ShoppingApi\Model\Body2 $body Item attributes (required)
     * @return Array of null, HTTP status code, HTTP response headers (array of strings)
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsItemIdPatchWithHttpInfo($id, $cart_id, $item_id, $body)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling apiContactsIdCartsCartIdItemsItemIdPatch');
        }
        // verify the required parameter 'cart_id' is set
        if ($cart_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $cart_id when calling apiContactsIdCartsCartIdItemsItemIdPatch');
        }
        // verify the required parameter 'item_id' is set
        if ($item_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $item_id when calling apiContactsIdCartsCartIdItemsItemIdPatch');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling apiContactsIdCartsCartIdItemsItemIdPatch');
        }
        // parse inputs
        $resourcePath = "/api/contacts/{id}/carts/{cartId}/items/{itemId}";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($cart_id !== null) {
            $resourcePath = str_replace(
                "{" . "cartId" . "}",
                $this->apiClient->getSerializer()->toPathValue($cart_id),
                $resourcePath
            );
        }
        // path params
        if ($item_id !== null) {
            $resourcePath = str_replace(
                "{" . "itemId" . "}",
                $this->apiClient->getSerializer()->toPathValue($item_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'PATCH',
                $queryParams,
                $httpBody,
                $headerParams,
                null,
                '/api/contacts/{id}/carts/{cartId}/items/{itemId}'
            );

            return array(null, $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
            }

            throw $e;
        }
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsPost
     *
     * Add an item to the given cart
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param \Postnord\ShoppingApi\Model\Body1 $body Item attributes (required)
     * @return \Postnord\ShoppingApi\Model\InlineResponse2013
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsPost($id, $cart_id, $body)
    {
        list($response) = $this->apiContactsIdCartsCartIdItemsPostWithHttpInfo($id, $cart_id, $body);
        return $response;
    }

    /**
     * Operation apiContactsIdCartsCartIdItemsPostWithHttpInfo
     *
     * Add an item to the given cart
     *
     * @param string $id Unique identifier for the contact (required)
     * @param string $cart_id Unique identifier for the cart (required)
     * @param \Postnord\ShoppingApi\Model\Body1 $body Item attributes (required)
     * @return Array of \Postnord\ShoppingApi\Model\InlineResponse2013, HTTP status code, HTTP response headers (array of strings)
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsCartIdItemsPostWithHttpInfo($id, $cart_id, $body)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling apiContactsIdCartsCartIdItemsPost');
        }
        // verify the required parameter 'cart_id' is set
        if ($cart_id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $cart_id when calling apiContactsIdCartsCartIdItemsPost');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling apiContactsIdCartsCartIdItemsPost');
        }
        // parse inputs
        $resourcePath = "/api/contacts/{id}/carts/{cartId}/items";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // path params
        if ($cart_id !== null) {
            $resourcePath = str_replace(
                "{" . "cartId" . "}",
                $this->apiClient->getSerializer()->toPathValue($cart_id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Postnord\ShoppingApi\Model\InlineResponse2013',
                '/api/contacts/{id}/carts/{cartId}/items'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Postnord\ShoppingApi\Model\InlineResponse2013', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Postnord\ShoppingApi\Model\InlineResponse2013', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

    /**
     * Operation apiContactsIdCartsPost
     *
     * Create a new cart for the given contact
     *
     * @param string $id Unique identifier for the contact (required)
     * @param \Postnord\ShoppingApi\Model\Body $body Cart attributes (required)
     * @return \Postnord\ShoppingApi\Model\InlineResponse2012
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsPost($id, $body)
    {
        list($response) = $this->apiContactsIdCartsPostWithHttpInfo($id, $body);
        return $response;
    }

    /**
     * Operation apiContactsIdCartsPostWithHttpInfo
     *
     * Create a new cart for the given contact
     *
     * @param string $id Unique identifier for the contact (required)
     * @param \Postnord\ShoppingApi\Model\Body $body Cart attributes (required)
     * @return Array of \Postnord\ShoppingApi\Model\InlineResponse2012, HTTP status code, HTTP response headers (array of strings)
     * @throws \Postnord\ShoppingApi\ApiException on non-2xx response
     */
    public function apiContactsIdCartsPostWithHttpInfo($id, $body)
    {
        // verify the required parameter 'id' is set
        if ($id === null) {
            throw new \InvalidArgumentException('Missing the required parameter $id when calling apiContactsIdCartsPost');
        }
        // verify the required parameter 'body' is set
        if ($body === null) {
            throw new \InvalidArgumentException('Missing the required parameter $body when calling apiContactsIdCartsPost');
        }
        // parse inputs
        $resourcePath = "/api/contacts/{id}/carts";
        $httpBody = '';
        $queryParams = array();
        $headerParams = array();
        $formParams = array();
        $_header_accept = $this->apiClient->selectHeaderAccept(array('application/json'));
        if (!is_null($_header_accept)) {
            $headerParams['Accept'] = $_header_accept;
        }
        $headerParams['Content-Type'] = $this->apiClient->selectHeaderContentType(array('application/json'));

        // path params
        if ($id !== null) {
            $resourcePath = str_replace(
                "{" . "id" . "}",
                $this->apiClient->getSerializer()->toPathValue($id),
                $resourcePath
            );
        }
        // default format to json
        $resourcePath = str_replace("{format}", "json", $resourcePath);

        // body params
        $_tempBody = null;
        if (isset($body)) {
            $_tempBody = $body;
        }

        // for model (json/xml)
        if (isset($_tempBody)) {
            $httpBody = $_tempBody; // $_tempBody is the method argument, if present
        } elseif (count($formParams) > 0) {
            $httpBody = $formParams; // for HTTP post (form)
        }
        // this endpoint requires OAuth (access token)
        if (strlen($this->apiClient->getConfig()->getAccessToken()) !== 0) {
            $headerParams['Authorization'] = 'Bearer ' . $this->apiClient->getConfig()->getAccessToken();
        }
        // make the API Call
        try {
            list($response, $statusCode, $httpHeader) = $this->apiClient->callApi(
                $resourcePath,
                'POST',
                $queryParams,
                $httpBody,
                $headerParams,
                '\Postnord\ShoppingApi\Model\InlineResponse2012',
                '/api/contacts/{id}/carts'
            );

            return array($this->apiClient->getSerializer()->deserialize($response, '\Postnord\ShoppingApi\Model\InlineResponse2012', $httpHeader), $statusCode, $httpHeader);
        } catch (ApiException $e) {
            switch ($e->getCode()) {
                case 201:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Postnord\ShoppingApi\Model\InlineResponse2012', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
                case 422:
                    $data = $this->apiClient->getSerializer()->deserialize($e->getResponseBody(), '\Postnord\ShoppingApi\Model\ValidationError', $e->getResponseHeaders());
                    $e->setResponseObject($data);
                    break;
            }

            throw $e;
        }
    }

}
