<?php
/**
 * ContactAddressResponseDeliverySchedule
 *
 * PHP version 5
 *
 * @category Class
 * @package  Postnord\ShoppingApi
 * @author   http://github.com/swagger-api/swagger-codegen
 * @license  http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * oAuth and Contacts APIs
 *
 * Api enpoints for oAuth and Contacts
 *
 * OpenAPI spec version: 1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Postnord\ShoppingApi\Model;

use \ArrayAccess;

/**
 * ContactAddressResponseDeliverySchedule Class Doc Comment
 *
 * @category    Class */
/** 
 * @package     Postnord\ShoppingApi
 * @author      http://github.com/swagger-api/swagger-codegen
 * @license     http://www.apache.org/licenses/LICENSE-2.0 Apache Licene v2
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ContactAddressResponseDeliverySchedule implements ArrayAccess
{
    /**
      * The original name of the model.
      * @var string
      */
    protected static $swaggerModelName = 'contactAddressResponse_delivery_schedule';

    /**
      * Array of property to type mappings. Used for (de)serialization
      * @var string[]
      */
    protected static $swaggerTypes = array(
        'monday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'tuesday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'wednesday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'thursday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'friday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'saturday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday',
        'sunday' => '\Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday'
    );

    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     * @var string[]
     */
    protected static $attributeMap = array(
        'monday' => 'monday',
        'tuesday' => 'tuesday',
        'wednesday' => 'wednesday',
        'thursday' => 'thursday',
        'friday' => 'friday',
        'saturday' => 'saturday',
        'sunday' => 'sunday'
    );

    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     * @var string[]
     */
    protected static $setters = array(
        'monday' => 'setMonday',
        'tuesday' => 'setTuesday',
        'wednesday' => 'setWednesday',
        'thursday' => 'setThursday',
        'friday' => 'setFriday',
        'saturday' => 'setSaturday',
        'sunday' => 'setSunday'
    );

    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     * @var string[]
     */
    protected static $getters = array(
        'monday' => 'getMonday',
        'tuesday' => 'getTuesday',
        'wednesday' => 'getWednesday',
        'thursday' => 'getThursday',
        'friday' => 'getFriday',
        'saturday' => 'getSaturday',
        'sunday' => 'getSunday'
    );

    public static function getters()
    {
        return self::$getters;
    }

    

    

    /**
     * Associative array for storing property values
     * @var mixed[]
     */
    protected $container = array();

    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['monday'] = isset($data['monday']) ? $data['monday'] : null;
        $this->container['tuesday'] = isset($data['tuesday']) ? $data['tuesday'] : null;
        $this->container['wednesday'] = isset($data['wednesday']) ? $data['wednesday'] : null;
        $this->container['thursday'] = isset($data['thursday']) ? $data['thursday'] : null;
        $this->container['friday'] = isset($data['friday']) ? $data['friday'] : null;
        $this->container['saturday'] = isset($data['saturday']) ? $data['saturday'] : null;
        $this->container['sunday'] = isset($data['sunday']) ? $data['sunday'] : null;
    }

    /**
     * show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalid_properties = array();
        return $invalid_properties;
    }

    /**
     * validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properteis are valid
     */
    public function valid()
    {
        return true;
    }


    /**
     * Gets monday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getMonday()
    {
        return $this->container['monday'];
    }

    /**
     * Sets monday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $monday
     * @return $this
     */
    public function setMonday($monday)
    {
        $this->container['monday'] = $monday;

        return $this;
    }

    /**
     * Gets tuesday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getTuesday()
    {
        return $this->container['tuesday'];
    }

    /**
     * Sets tuesday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $tuesday
     * @return $this
     */
    public function setTuesday($tuesday)
    {
        $this->container['tuesday'] = $tuesday;

        return $this;
    }

    /**
     * Gets wednesday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getWednesday()
    {
        return $this->container['wednesday'];
    }

    /**
     * Sets wednesday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $wednesday
     * @return $this
     */
    public function setWednesday($wednesday)
    {
        $this->container['wednesday'] = $wednesday;

        return $this;
    }

    /**
     * Gets thursday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getThursday()
    {
        return $this->container['thursday'];
    }

    /**
     * Sets thursday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $thursday
     * @return $this
     */
    public function setThursday($thursday)
    {
        $this->container['thursday'] = $thursday;

        return $this;
    }

    /**
     * Gets friday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getFriday()
    {
        return $this->container['friday'];
    }

    /**
     * Sets friday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $friday
     * @return $this
     */
    public function setFriday($friday)
    {
        $this->container['friday'] = $friday;

        return $this;
    }

    /**
     * Gets saturday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getSaturday()
    {
        return $this->container['saturday'];
    }

    /**
     * Sets saturday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $saturday
     * @return $this
     */
    public function setSaturday($saturday)
    {
        $this->container['saturday'] = $saturday;

        return $this;
    }

    /**
     * Gets sunday
     * @return \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday
     */
    public function getSunday()
    {
        return $this->container['sunday'];
    }

    /**
     * Sets sunday
     * @param \Postnord\ShoppingApi\Model\ContactAddressResponseDeliveryScheduleMonday $sunday
     * @return $this
     */
    public function setSunday($sunday)
    {
        $this->container['sunday'] = $sunday;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     * @param  integer $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     * @param  integer $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     * @param  integer $offset Offset
     * @param  mixed   $value  Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     * @param  integer $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(\Postnord\ShoppingApi\ObjectSerializer::sanitizeForSerialization($this), JSON_PRETTY_PRINT);
        }

        return json_encode(\Postnord\ShoppingApi\ObjectSerializer::sanitizeForSerialization($this));
    }
}


